package com.telerikacademi.beertag.navigation;

import android.app.Activity;

import androidx.fragment.app.Fragment;

public interface NavigationHost {

    void navigateToFragment(Fragment fragment, boolean addToBackstack);

    void navigateToActivity();
}
