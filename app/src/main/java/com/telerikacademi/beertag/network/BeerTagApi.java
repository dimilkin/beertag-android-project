package com.telerikacademi.beertag.network;

import com.telerikacademi.beertag.models.BeerModel;
import com.telerikacademi.beertag.models.BeerStyleModel;
import com.telerikacademi.beertag.models.BrewaryCountryModel;
import com.telerikacademi.beertag.models.BrewaryModel;
import com.telerikacademi.beertag.models.UserModel;
import com.telerikacademi.beertag.models.modelDtos.BeerDto;

import java.util.List;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BeerTagApi {

    @GET("api/beers")
    Single<List<BeerModel>> getBeers();

    @GET("api/users/{user-id}/whished")
    Single<List<BeerModel>> getWihedBeers(@Path("user-id") int userId);

    @GET("api/users/{user-id}/drunk")
    Single<List<BeerModel>> getDrunkBeers(@Path("user-id") int userId);

    @POST("api/beers/")
    Call<BeerDto> saveBeerToDb(
            @Header("Authorization") int ownerId,
            @Body BeerDto beerDto);

    @PUT("api/users/{user-id}/whished/{beer-id}")
    Call<BeerDto> saveBeerWishlist(
            @Path("user-id") int userId,
            @Path("beer-id") int beerId);

    @PUT("api/users/{user-id}/drunk/{beer-id}")
    Call<BeerDto> saveBeerDrunkList(
            @Path("user-id") int userId,
            @Path("beer-id") int beerId);

}



