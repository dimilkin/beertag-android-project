package com.telerikacademi.beertag.view_auth;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;

import com.telerikacademi.beertag.navigation.NavigationHost;
import com.telerikacademi.beertag.R;
import com.telerikacademi.beertag.view_main.MainActivity;

public class LogRegActivity extends AppCompatActivity implements NavigationHost {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_reg);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.logRegContainer, new LoginFragment())
                    .commit();
        }
    }


    @Override
    public void navigateToFragment(Fragment fragment, boolean addToBackstack) {
        FragmentTransaction transaction =
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.logRegContainer, fragment);

        if (addToBackstack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }

    @Override
    public void navigateToActivity() {
        Intent intent = new Intent(LogRegActivity.this, MainActivity.class);
        startActivity(intent);
    }
}