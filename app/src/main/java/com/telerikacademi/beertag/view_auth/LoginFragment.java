package com.telerikacademi.beertag.view_auth;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.telerikacademi.beertag.databinding.FragmentLoginBinding;
import com.telerikacademi.beertag.navigation.NavigationHost;
import com.telerikacademi.beertag.R;


public class LoginFragment extends Fragment {

    TextInputLayout passwordTextInput;
    TextInputEditText passwordEditText;
    MaterialButton signIn, signUp;


    public LoginFragment() {
        // Required empty public constructor
    }

    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup logRegContainer, Bundle savedInstanceState) {
        FragmentLoginBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, logRegContainer, false);

        passwordTextInput = binding.passwordTextInput;
        passwordEditText = binding.passwordEditText;
        signIn = binding.signInButton;
        signUp = binding.signUpButton;

        signIn.setOnClickListener(view -> {
            if (!isPasswordValid(passwordEditText.getText())) {
                passwordTextInput.setError(getString(R.string.fragment_login));
            } else {
                passwordTextInput.setError(null);
                ((NavigationHost) getActivity()).navigateToActivity();
            }
        });

        signUp.setOnClickListener(view -> {
            ((NavigationHost) getActivity()).navigateToFragment(new RegisterFragment(), false);
        });


        passwordEditText.setOnKeyListener((view1, i, keyEvent) -> {
            if (isPasswordValid(passwordEditText.getText())) {
                passwordTextInput.setError(null); //Clear the error
            }
            return false;
        });

        return binding.getRoot();
    }

    /*
    Dummy checking for password - if it is larger than 3 chars - it is valid. Else an error is shown.
     */
    private boolean isPasswordValid(@Nullable Editable text) {
        return text != null && text.length() >= 3;
    }
}