package com.telerikacademi.beertag.view_auth;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.telerikacademi.beertag.R;
import com.telerikacademi.beertag.databinding.FragmentRegisterBinding;
import com.telerikacademi.beertag.navigation.NavigationHost;

public class RegisterFragment extends Fragment {

    public RegisterFragment() {
    }

    private Button nextButton, backButton;
    private EditText nameField, mailField, passField, repeatPassField;
    private TextView iHaveAnAccount;
    ImageView background;

    private int stage = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentRegisterBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);

        nameField = binding.regNameTextField;
        mailField = binding.regMailTextField;
        passField = binding.regPasswordTextField;
        repeatPassField = binding.regRepeatPasswordTextField;
        background = binding.registerImageBackground;
        iHaveAnAccount = binding.iHaveAnAccount;

        nextButton = binding.regNextBtn;
        backButton = binding.regBackBtn;

        startRegistration(stage);

        nextButton.setOnClickListener(button -> {
            if (stage < 5) {
                stage++;
                startRegistration(stage);
            }
        });

        backButton.setOnClickListener(button -> {
            if (stage > 1) {
                stage--;
                startRegistration(stage);
            }
        });

        iHaveAnAccount.setOnClickListener(text -> {
            ((NavigationHost) getActivity()).navigateToFragment(new LoginFragment(), false);
        });


        return binding.getRoot();
    }


    private void startRegistration(int curretnStage) {

        switch (curretnStage) {
            case 1:
                firstStage();
                break;
            case 2:
                secondStage();
                break;
            case 3:
                thirdStage();
                break;
            case 4:
                fourthStage();
                break;
            case 5:
                completeRegistration();
                break;
            default:
                Toast.makeText(getContext(), "Registration Failed", Toast.LENGTH_SHORT).show();
        }
    }


    private void firstStage() {

        background.setBackgroundResource(R.drawable.reg_background1);
        backButton.setVisibility(View.GONE);
        nameField.setVisibility(View.VISIBLE);
        mailField.setVisibility(View.GONE);
        passField.setVisibility(View.GONE);
        repeatPassField.setVisibility(View.GONE);
        nextButton.setText("Next");


    }

    private void secondStage() {

        background.setBackgroundResource(R.drawable.reg_background2);
        backButton.setVisibility(View.VISIBLE);
        nameField.setVisibility(View.GONE);
        mailField.setVisibility(View.VISIBLE);
        passField.setVisibility(View.GONE);
        repeatPassField.setVisibility(View.GONE);
        nextButton.setText("Next");
    }

    private void thirdStage() {

        background.setBackgroundResource(R.drawable.reg_background3);
        backButton.setVisibility(View.VISIBLE);
        nameField.setVisibility(View.GONE);
        mailField.setVisibility(View.GONE);
        passField.setVisibility(View.VISIBLE);
        repeatPassField.setVisibility(View.GONE);
        nextButton.setText("Next");
    }

    private void fourthStage() {

        background.setBackgroundResource(R.drawable.reg_background4);
        backButton.setVisibility(View.VISIBLE);
        nameField.setVisibility(View.GONE);
        mailField.setVisibility(View.GONE);
        passField.setVisibility(View.GONE);
        repeatPassField.setVisibility(View.VISIBLE);
        nextButton.setText("Register");
    }


    /*
    Dummy registration. In real scenario a registration logic should be inserted here.
    The user would be redirected to the log-in fragment.
     */
    private void completeRegistration() {
        Toast.makeText(getContext(), " Registration Successful", Toast.LENGTH_SHORT).show();
        ((NavigationHost) getActivity()).navigateToActivity();

    }

}