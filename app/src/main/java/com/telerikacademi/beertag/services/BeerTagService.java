package com.telerikacademi.beertag.services;


import com.telerikacademi.beertag.models.BeerModel;
import com.telerikacademi.beertag.models.modelDtos.BeerDto;
import com.telerikacademi.beertag.network.BeerTagApi;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BeerTagService {

    public static final int HARDCODED_USER = 5;


    public static final String BASE_URL = "http://0cb389ee13c8.ngrok.io/";
    private static BeerTagService instance;

    private BeerTagApi api = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(BeerTagApi.class);

    private BeerTagService() {
    }

    public static BeerTagService getInstance() {
        if (instance == null) {
            instance = new BeerTagService();
        }
        return instance;
    }


    public Single<List<BeerModel>> getBeers() {
        return api.getBeers();
    }
    public Single<List<BeerModel>> getWanted() {
        return api.getWihedBeers(HARDCODED_USER);
    }
    public Single<List<BeerModel>> getDrunk() {
        return api.getDrunkBeers(HARDCODED_USER);
    }
    public Call<BeerDto> saveBeerInfo(BeerDto dto){
        return api.saveBeerToDb(HARDCODED_USER,  dto);
    }
    public Call<BeerDto> saveBeerToWIshList(int beerID){
        return api.saveBeerWishlist(HARDCODED_USER,  beerID);
    }
    public Call<BeerDto> saveBeerToDrinkList(int beerId){
        return api.saveBeerDrunkList(HARDCODED_USER,  beerId);
    }


}
