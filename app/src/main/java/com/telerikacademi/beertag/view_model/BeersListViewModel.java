package com.telerikacademi.beertag.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.telerikacademi.beertag.models.BeerModel;
import com.telerikacademi.beertag.models.modelDtos.BeerDto;
import com.telerikacademi.beertag.services.BeerTagService;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BeersListViewModel extends ViewModel {

    public MutableLiveData <List<BeerModel>> allBeersList = new MutableLiveData<List<BeerModel>>();
    public MutableLiveData <List<BeerModel>> wishedBeerList = new MutableLiveData<List<BeerModel>>();
    public MutableLiveData <List<BeerModel>> drinkBeerList = new MutableLiveData<List<BeerModel>>();
    public MutableLiveData <Boolean> isLoading = new MutableLiveData<Boolean>();
    public MutableLiveData <Boolean> loadingError = new MutableLiveData<Boolean>();

    private BeerTagService beerService = BeerTagService.getInstance();

    private CompositeDisposable disposable = new CompositeDisposable();

    public void refreshBeers(){
        fetchBeers(allBeersList, beerService.getBeers());
    }

    public void getWhishedBeers(){
        fetchBeers(wishedBeerList, beerService.getWanted());
    }

    public void getDrinkBeers(){
        fetchBeers(drinkBeerList, beerService.getDrunk());
    }

    private void fetchBeers(MutableLiveData<List<BeerModel>>list, Single<List<BeerModel>> service) {
        isLoading.setValue(true);
        disposable.add(
        service
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<BeerModel>>() {
                    @Override
                    public void onSuccess(List<BeerModel> beerModels) {
                        list.setValue(beerModels);
                        loadingError.setValue(false);
                        isLoading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        isLoading.setValue(false);
                        loadingError.setValue(true);
                        e.printStackTrace();
                    }
                })
        );

    }

    public void addBeerToWishList(int beerID){
        addBeerToList(beerService.saveBeerToWIshList(beerID));
    }
{}
    public void addBeerToDrinkList(int beerId){
        addBeerToList(beerService.saveBeerToDrinkList(beerId));
    }

    private void addBeerToList(Call<BeerDto> call) {
        call.enqueue(new Callback<BeerDto>() {
            @Override
            public void onResponse(Call<BeerDto> call, Response<BeerDto> response) {
            }

            @Override
            public void onFailure(Call<BeerDto> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        disposable.clear();
    }
}
