package com.telerikacademi.beertag.models;

import com.google.gson.annotations.SerializedName;

public class BrewaryModel {

    private int id;

    @SerializedName("producer")
    private String producer;

    public BrewaryModel(int id, String brewaryName) {
        this.id = id;
        this.producer = brewaryName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    @Override
    public String toString() {
        return producer;
    }
}

