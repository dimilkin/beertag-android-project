package com.telerikacademi.beertag.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "beer_styles")
public class BeerStyleModel {

    @PrimaryKey
    private int id;
    @SerializedName("name")
    private String style;

    public BeerStyleModel(int id, String style) {
        this.id = id;
        this.style = style;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return  style ;
    }
}
