package com.telerikacademi.beertag.models;

import com.google.gson.annotations.SerializedName;

public class PictureModel {

    @SerializedName("id")
    private int id;

    @SerializedName("pictureName")
    private String pictureName;

    @SerializedName("pictureLocation")
    private String pictureLocation;

    public PictureModel(int id, String pictureName) {
        this.id = id;
        this.pictureName = pictureName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }

    public String getPictureLocation() {
        return pictureLocation;
    }

    public void setPictureLocation(String pictureLocation) {
        this.pictureLocation = pictureLocation;
    }
}
