package com.telerikacademi.beertag.models;

import com.google.gson.annotations.SerializedName;

public class BeerRateModel {

   private int id;
    @SerializedName("rating")
    private double rate;
    @SerializedName("evaluation")
    private String comment;

    public BeerRateModel() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

}
