package com.telerikacademi.beertag.models;

public class BrewaryCountryModel {

    private int id;
    private String country;

    public BrewaryCountryModel(int id, String countryName) {
        this.id = id;
        this.country = countryName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return country;
    }
}
