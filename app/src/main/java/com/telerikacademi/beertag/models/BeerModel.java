package com.telerikacademi.beertag.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BeerModel {

    private int id;
    private String name;
    private String description;
    private double abv;

    @SerializedName("beerPicture")
    private PictureModel picture;
    private BrewaryModel brewary;
    private BeerStyleModel beerStyle;
    private UserModel createdByUser;

    @SerializedName("ratings")
    private List<BeerRateModel> ratings = new ArrayList<>();

    public BeerModel() {
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public PictureModel getPicture() {
        return picture;
    }

    public void setPicture(PictureModel picture) {
        this.picture = picture;
    }

    public BrewaryModel getBrewary() {
        return brewary;
    }

    public void setBrewary(BrewaryModel brewary) {
        this.brewary = brewary;
    }

    public UserModel getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(UserModel createdByUser) {
        this.createdByUser = createdByUser;
    }

    public List<BeerRateModel> getRatings() {
        return ratings;
    }

    public void setRatings(List<BeerRateModel> ratings) {
        this.ratings = ratings;
    }

    public BeerStyleModel getBeerStyle() {
        return beerStyle;
    }

    public void setBeerStyle(BeerStyleModel beerStyle) {
        this.beerStyle = beerStyle;
    }

    public float getAverageRating (){
        return (float) getRatings().stream()
                .mapToDouble(BeerRateModel::getRate)
                .average()
                .orElse(5);
    }
}
