package com.telerikacademi.beertag.view_main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.telerikacademi.beertag.R;
import com.telerikacademi.beertag.databinding.ActivityAddEdditBeerBinding;
import com.telerikacademi.beertag.models.BeerStyleModel;
import com.telerikacademi.beertag.models.BrewaryCountryModel;
import com.telerikacademi.beertag.models.BrewaryModel;
import com.telerikacademi.beertag.models.modelDtos.BeerDto;
import com.telerikacademi.beertag.services.BeerTagService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.telerikacademi.beertag.services.BeerTagService.HARDCODED_USER;

public class AddBeerActivity extends AppCompatActivity {

    private static final String TAG = " System Info : ";
    private static final int SELECT_PICTURE = 1;

    EditText beerNameField, beerDescriptionField, beerTagsField, editBeerAbvField;
    Button saveBeerButton;
    ImageButton beerImageButton;
    Spinner beerStylesSpiner, beerCountrySpiner, beerBrewerySpinner;

    private BeerTagService service;

    String beerNameValue, descriptionValue, abvValue;
    int beerStyleId, beerCountryId, beerBreweryId;
    Uri fileUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityAddEdditBeerBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_add_eddit_beer);

        service = BeerTagService.getInstance();
        beerNameField = binding.editBeerNameField;
        beerDescriptionField = binding.editDescriptionTextField;
        beerTagsField = binding.editTagsTextField;
        saveBeerButton = binding.saveCurrentBeerBtn;
        beerImageButton = binding.addBeerImageButton;
        editBeerAbvField = binding.editBeerAbvField;
        beerStylesSpiner = (Spinner) binding.chooseStyleSpinner;
        beerCountrySpiner = binding.chooseCountrySpinner;
        beerBrewerySpinner = binding.chooseBrewerySpinner;

        List<BeerStyleModel> stylesList = new ArrayList<>();
        List<BrewaryModel> brewariesList = new ArrayList<>();
        List<BrewaryCountryModel> contriesList = new ArrayList<>();

        generateDummyData(stylesList, contriesList, brewariesList);

        ArrayAdapter<BeerStyleModel> styleAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, stylesList);
        styleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        beerStylesSpiner.setAdapter(styleAdapter);

        ArrayAdapter<BrewaryCountryModel> countriesAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, contriesList);
        countriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        beerCountrySpiner.setAdapter(countriesAdapter);

        ArrayAdapter<BrewaryModel> breweriesAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, brewariesList);
        breweriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        beerBrewerySpinner.setAdapter(breweriesAdapter);

        beerStylesSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BeerStyleModel style = (BeerStyleModel) parent.getSelectedItem();
                beerStyleId = style.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        beerCountrySpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BrewaryCountryModel model = (BrewaryCountryModel) parent.getSelectedItem();
                beerCountryId = model.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        beerBrewerySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BrewaryModel model = (BrewaryModel) parent.getSelectedItem();
                beerBreweryId = model.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        beerImageButton.setOnClickListener(this::fetchImageFromGallery);


        saveBeerButton.setOnClickListener(v -> {
            beerNameValue = beerNameField.getText().toString();
            descriptionValue = beerDescriptionField.getText().toString();
            abvValue = editBeerAbvField.getText().toString();

            BeerDto dto = new BeerDto();
            dto.setName(beerNameValue);
            dto.setAbv(Double.parseDouble(abvValue));
            dto.setCreated_by_user_id(HARDCODED_USER);
            dto.setDescription(descriptionValue);
            dto.setProducer_id(beerBreweryId);
            dto.setStyleId(beerStyleId);

            sendBeerInfo(dto);

            Toast.makeText(AddBeerActivity.this, " Beer Added ", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(AddBeerActivity.this, MainActivity.class);
            startActivity(intent);
        });

    }


    private void sendBeerInfo(BeerDto dto) {

        service.saveBeerInfo(dto).enqueue(new Callback<BeerDto>() {
            @Override
            public void onResponse(Call<BeerDto> call, Response<BeerDto> response) {
                Log.i(TAG, "Beer successfully sent");

            }

            @Override
            public void onFailure(Call<BeerDto> call, Throwable t) {
                Log.i(TAG, "An error occured");
            }
        });
    }


    void fetchImageFromGallery(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
          /*
            A picture is selected from the gallery. It won't be used though.
          */
                fileUri = data.getData();
            }
        }
    }


    private void generateDummyData(List<BeerStyleModel> stylesList, List<BrewaryCountryModel> contriesList, List<BrewaryModel> brewariesList) {
        BeerStyleModel style1 = new BeerStyleModel(1, "Dark ");
        BeerStyleModel style2 = new BeerStyleModel(3, "Amber");
        BeerStyleModel style4 = new BeerStyleModel(4, "Blonde");
        BeerStyleModel style3 = new BeerStyleModel(5, "Brown");
        BeerStyleModel style5 = new BeerStyleModel(6, "Cream");
        BeerStyleModel style6 = new BeerStyleModel(8, "Strong");
        BeerStyleModel style7 = new BeerStyleModel(9, "Pale");
        BeerStyleModel style9 = new BeerStyleModel(10, "Red");
        BeerStyleModel style8 = new BeerStyleModel(11, "Indian Pale Ale");

        stylesList.add(style1);
        stylesList.add(style2);
        stylesList.add(style3);
        stylesList.add(style4);
        stylesList.add(style5);
        stylesList.add(style6);
        stylesList.add(style7);
        stylesList.add(style8);
        stylesList.add(style9);


        BrewaryCountryModel countryModel1 = new BrewaryCountryModel(1, "Belgium");
        BrewaryCountryModel countryModel2 = new BrewaryCountryModel(2, "USA");

        contriesList.add(countryModel1);
        contriesList.add(countryModel2);

        BrewaryModel brewary1 = new BrewaryModel(1, "Duvel Moortgat Brewery");
        BrewaryModel brewary2 = new BrewaryModel(1, "Bell’s Brewery");

        brewariesList.add(brewary1);
        brewariesList.add(brewary2);
    }
}