package com.telerikacademi.beertag.view_main;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.telerikacademi.beertag.R;
import com.telerikacademi.beertag.models.BeerModel;
import com.telerikacademi.beertag.services.BeerTagService;

import java.util.List;

public class BeerListAdapter extends ListAdapter<BeerModel, BeerListAdapter.BeerViewHolder> {

    private OnWishButtonClickListener wishButtonClickListener;
    private OnAddToDrunkClickListener drunkButtonClickListener;
    private AdapterClickListener adapterClickListener;


    public BeerListAdapter(List<BeerModel> beers) {
        super(DIFF_CALLBACK);

    }

    private static final DiffUtil.ItemCallback<BeerModel> DIFF_CALLBACK = new DiffUtil.ItemCallback<BeerModel>() {
        @Override
        public boolean areItemsTheSame(@NonNull BeerModel oldItem, @NonNull BeerModel newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull BeerModel oldItem, @NonNull BeerModel newItem) {
            return oldItem.getName().equals(newItem.getName());

        }
    };


    @NonNull
    @Override
    public BeerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.beer_card, parent, false);
        return new BeerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BeerViewHolder holder, int position) {

        BeerModel currentBeer = getItem(position);

        String picAddress = BeerTagService.BASE_URL + "/files/" +  currentBeer.getPicture().getPictureName();

        holder.beerName.setText(currentBeer.getName());
        double abv = currentBeer.getAbv();
        holder.beerAbv.setText(Double.toString(abv));
        holder.beerRating.setRating(currentBeer.getAverageRating());
        GlideConfig.loadImage(holder.beerImage, picAddress);

    }


    class BeerViewHolder extends RecyclerView.ViewHolder{

        private ImageView beerImage;
        private TextView beerName, beerAbv;
        private RatingBar beerRating;
        private ImageButton drunkBtn, wishBtn;

        public BeerViewHolder(@NonNull View itemView) {
            super(itemView);

            beerImage = itemView.findViewById(R.id.beerImageView);
            beerName = itemView.findViewById(R.id.beerNameTextView);
            beerAbv = itemView.findViewById(R.id.beerAbvTextView);
            beerRating = itemView.findViewById(R.id.beerRatingBar);
            drunkBtn = itemView.findViewById(R.id.drunkButton);
            wishBtn = itemView.findViewById(R.id.wishedButton);

            itemView.setOnClickListener(view-> {
                int position = getAdapterPosition();
                if (adapterClickListener != null && position != RecyclerView.NO_POSITION) {
                    adapterClickListener.onAdapterClick(getItem(position));
                }
            });

            wishBtn.setOnClickListener(view -> {
                int position = getAdapterPosition();
                if (wishButtonClickListener != null && position != RecyclerView.NO_POSITION) {
                    wishButtonClickListener.onWishButtonClickListener(wishBtn);
                }
            });

            drunkBtn.setOnClickListener( view->{
                int position = getAdapterPosition();
                if (drunkButtonClickListener != null && position != RecyclerView.NO_POSITION) {
                    drunkButtonClickListener.onDrunkButtonClickListener(drunkBtn);
                }
            });


        }
    }

    public interface OnWishButtonClickListener {
        void onWishButtonClickListener(ImageButton imageButton);
    }

    public interface OnAddToDrunkClickListener {
        void onDrunkButtonClickListener(ImageButton imageButton);
    }

    public interface AdapterClickListener {
        void onAdapterClick(BeerModel beerModel);
    }


    public void setWishButtonClickListener(OnWishButtonClickListener imageButtonClickListener) {
        this.wishButtonClickListener = imageButtonClickListener;
    }

    public void setDrunkButtonClickListener(OnAddToDrunkClickListener imageButtonClickListener) {
        this.drunkButtonClickListener = imageButtonClickListener;
    }

    public void setAdapterClickListener(AdapterClickListener adapterClickListener){
        this.adapterClickListener = adapterClickListener;
    }

}
