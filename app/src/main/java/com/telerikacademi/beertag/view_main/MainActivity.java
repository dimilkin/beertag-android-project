package com.telerikacademi.beertag.view_main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.telerikacademi.beertag.R;
import com.telerikacademi.beertag.databinding.ActivityMainBinding;
import com.telerikacademi.beertag.models.BeerModel;
import com.telerikacademi.beertag.view_model.BeersListViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int ADD_CHILD_REQUEST = 1;
    private static final String EXTRA_ID ="com.telerikacademi.beertag.EXTRA_ID" ;
    private static final String EXTRA_NAME = "com.telerikacademi.beertag.EXTRA_NAME";
    private static final String EXTRA_DESCRIPTION = "com.telerikacademi.beertag.EXTRA_DESCRIPTION";
    private static final String EXTRA_ABV = "com.telerikacademi.beertag.EXTRA_ABV";
    private static final String EXTRA_STYLE = "com.telerikacademi.beertag.EXTRA_STYLE";
    private static final String EXTRA_BREWERY = "com.telerikacademi.beertag.EXTRA_BREWERY";
    private static final String EXTRA_PICTURE ="com.telerikacademi.beertag.EXTRA_PICTURE" ;
    private static final String EXTRA_AUTHOR = "com.telerikacademi.beertag.EXTRA_AUTHOR";
    private static final String EXTRA_RATING ="com.telerikacademi.beertag.EXTRA_RATING";


    RecyclerView beersRecyclerView;
    TextView loadingErrorText;
    ProgressBar beersProgressLoadingbar;
    SwipeRefreshLayout refreshBeersSwipe;
    FloatingActionButton floatButton;
    Button wishedBeers, drinkBeers;
    boolean showMenu;

    private BeersListViewModel beersViewModel;
    private BeerListAdapter beersAdapter = new BeerListAdapter(new ArrayList<>());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        showMenu = false;

        beersRecyclerView = binding.beersRecyclerView;
        Toolbar myToolbar = binding.appBar;
        setSupportActionBar(myToolbar);
        myToolbar.bringToFront();

        beersRecyclerView.bringToFront();
        beersRecyclerView.setBackgroundResource(R.drawable.backdrop_shape);

        beersProgressLoadingbar = binding.beersLoadingProgressBar;
        beersProgressLoadingbar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorRed), android.graphics.PorterDuff.Mode.MULTIPLY);

        refreshBeersSwipe = binding.mainSwipeRefreshLayout;
        floatButton = binding.floatingActionButton;
        loadingErrorText = binding.beersLoadingErrorText;

        /*
        On swipe a new call to the API is made and new information is loaded in the recycler view
        */
        refreshBeersSwipe.setOnRefreshListener(() -> {
            beersViewModel.refreshBeers();
            refreshBeersSwipe.setRefreshing(false);
        });

        /*
        Dummy button for showing UI and changes on button click
        */
        beersAdapter.setWishButtonClickListener(wishBtn -> {
                    wishBtn.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_wished_full));
                }
        );
        /*
        Dummy button for showing UI and changes on button click
        */
        beersAdapter.setDrunkButtonClickListener(drunkBtn -> {
            drunkBtn.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_drunk_beer));
                }
        );

        /*
        On click on any item from the adapter a new activity shows the details of the selected item
        */
        beersAdapter.setAdapterClickListener( beerModel -> {
            Intent intent = new Intent(MainActivity.this, BeerInfoActivity.class);
            intent.putExtra(MainActivity.EXTRA_ID, beerModel.getId());
            intent.putExtra(MainActivity.EXTRA_NAME, beerModel.getName());
            intent.putExtra(MainActivity.EXTRA_DESCRIPTION, beerModel.getDescription());
            intent.putExtra(MainActivity.EXTRA_ABV, beerModel.getAbv());
            intent.putExtra(MainActivity.EXTRA_PICTURE, beerModel.getPicture().getPictureLocation());
            intent.putExtra(MainActivity.EXTRA_RATING, beerModel.getAverageRating());

            startActivity(intent);
        });

        /*
        Floating button navigates to activity for creating a new beer and sending it to the DB.
        */
        floatButton.setOnClickListener( view -> {
          navigateToAddEditActivity();
        });


        beersViewModel = ViewModelProviders.of(this).get(BeersListViewModel.class);
        beersViewModel.refreshBeers();

        beersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        beersRecyclerView.setAdapter(beersAdapter);

        observAllBeersViewModel(beersViewModel.allBeersList);

        /*
        On click a call is made to the API. A response is returned with the beers, that are in the "wished list"
        of the user. Those beers are populated in the recycler view
         */
        wishedBeers = binding.whishedBeersButton;
        wishedBeers.setOnClickListener(b ->{
            beersViewModel.getWhishedBeers();
            observAllBeersViewModel(beersViewModel.wishedBeerList);
            findViewById(R.id.linearLayout).setVisibility(View.GONE);
        });

        /*
        On click a call is made to the API. A response is returned with the beers, that are in the "drink list"
        of the user. Those beers are populated in the recycler view
        */
        drinkBeers = binding.drinkedBeersButon;
        drinkBeers.setOnClickListener(b ->{
            beersViewModel.getDrinkBeers();
            observAllBeersViewModel(beersViewModel.drinkBeerList);
            findViewById(R.id.linearLayout).setVisibility(View.GONE);
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_bar, menu);
        return true;
    }

    private void observAllBeersViewModel(MutableLiveData<List<BeerModel>> beersList) {

        beersList.observe(this, beerModels -> {
            if (beerModels != null) {
                beersRecyclerView.setVisibility(View.VISIBLE);
                beersAdapter.submitList(beerModels);
            }
        });
        beersViewModel.loadingError.observe(this, isError -> {
            if (isError != null) {
                loadingErrorText.setVisibility(isError ? View.VISIBLE : View.GONE);
            }
        });

        beersViewModel.isLoading.observe(this, isLoading -> {
            if (isLoading != null) {
                beersProgressLoadingbar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
                if (isLoading) {
                    beersRecyclerView.setVisibility(View.GONE);
                    loadingErrorText.setVisibility(View.GONE);
                }
            }
        });

    }

    private void navigateToAddEditActivity() {
        Intent intent = new Intent(MainActivity.this, AddBeerActivity.class);
        startActivityForResult(intent, ADD_CHILD_REQUEST);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                // User chose the "Settings" item, show the app settings UI...
                return true;

            case R.id.filter:
                if (!showMenu) {
                    findViewById(R.id.linearLayout).setVisibility(View.VISIBLE);
                    showMenu = true;
                }else {
                    findViewById(R.id.linearLayout).setVisibility(View.GONE);
                    showMenu = false;
                }
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}