package com.telerikacademi.beertag.view_main;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.telerikacademi.beertag.R;

public class GlideConfig {

    public static void loadImage (ImageView view, String url){
        RequestOptions options = new RequestOptions()
                .error(R.drawable.defult_beer);

        Glide.with(view.getContext())
                .setDefaultRequestOptions(options)
                .load(url)
                .override(250 ,250)
                .into(view);
    }

}
