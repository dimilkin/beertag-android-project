package com.telerikacademi.beertag.view_main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.telerikacademi.beertag.R;
import com.telerikacademi.beertag.databinding.ActivityBeerInfoBinding;
import com.telerikacademi.beertag.models.modelDtos.BeerDto;
import com.telerikacademi.beertag.services.BeerTagService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BeerInfoActivity extends AppCompatActivity {
    private static final String EXTRA_ID ="com.telerikacademi.beertag.EXTRA_ID" ;
    private static final String EXTRA_NAME = "com.telerikacademi.beertag.EXTRA_NAME";
    private static final String EXTRA_DESCRIPTION = "com.telerikacademi.beertag.EXTRA_DESCRIPTION";
    private static final String EXTRA_ABV = "com.telerikacademi.beertag.EXTRA_ABV";
    private static final String EXTRA_STYLE = "com.telerikacademi.beertag.EXTRA_STYLE";
    private static final String EXTRA_BREWERY = "com.telerikacademi.beertag.EXTRA_BREWERY";
    private static final String EXTRA_PICTURE ="com.telerikacademi.beertag.EXTRA_PICTURE" ;
    private static final String EXTRA_RATING = "com.telerikacademi.beertag.EXTRA_RATING";

    TextView beerNameField, descriptionField, abvField;
    RatingBar rateInfoBar, userrateingOption;
    ImageView beerImage;
    ImageButton addToWishedButton, addToConsumedButton;
    Button rateMeButton;
    BeerTagService beerService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBeerInfoBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_beer_info);
        beerService = BeerTagService.getInstance();


        beerNameField = dataBinding.beerInfoNameField;
        descriptionField = dataBinding.beerInfoDescriptionField;
        abvField = dataBinding.beerInfoAbv;
        rateInfoBar = dataBinding.beerInfoRating;
        userrateingOption = dataBinding.beerInfoUserRateBar;
        beerImage = dataBinding.beerInfoImageView;
        rateMeButton = dataBinding.beerInfoRateButton;
        addToWishedButton = dataBinding.beerInfoWishedButton;
        addToConsumedButton = dataBinding.beerInfoDrunkButton;


        Intent intent = getIntent();
        final int beerId = intent.getIntExtra(EXTRA_ID, 0);
        if (intent.hasExtra(EXTRA_ID)) {
            beerNameField.setText(intent.getStringExtra(EXTRA_NAME));
            descriptionField.setText(intent.getStringExtra(EXTRA_DESCRIPTION));
            abvField.setText(Double.toString(intent.getDoubleExtra(EXTRA_ABV, 0)));
            GlideConfig.loadImage(beerImage, (intent.getStringExtra(EXTRA_PICTURE)));
            rateInfoBar.setRating(intent.getFloatExtra(EXTRA_RATING, 0));
        }

        addToWishedButton.setOnClickListener(view ->{
            addBeerToWishList(beerId);
            addToWishedButton.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_wished_full));
        });

        addToConsumedButton.setOnClickListener(view ->{
            addBeerToDrinkList(beerId);
            addToConsumedButton.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_drunk_beer));
        });


    }

    public void addBeerToWishList(int beerID){
        addBeerToList(beerService.saveBeerToWIshList(beerID));
    }
    {}
    public void addBeerToDrinkList(int beerId){
        addBeerToList(beerService.saveBeerToDrinkList(beerId));
    }

    private void addBeerToList(Call<BeerDto> call) {
        call.enqueue(new Callback<BeerDto>() {
            @Override
            public void onResponse(Call<BeerDto> call, Response<BeerDto> response) {
            }

            @Override
            public void onFailure(Call<BeerDto> call, Throwable t) {

            }
        });
    }
}