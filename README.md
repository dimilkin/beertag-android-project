# BeerTag Android Project

### BeerTag is a mobile app for Android. It was developed as an addition to the BeerTag web project for Telerik Academy.

## Table of Content

- [General Info](#general-info)
- [Technologies](#technologies)
- [Features](#features)


## General Info
BeerTag was developed as an addition to the web project BeerTag for Telerik Academy. The main purpose of the application is to demonstrate how the users of the web application can have access to the information on the platform and be able to interact with it. The web platform provides functionalities that allow users to view information about beers, to sort and search beers, to perform CRUD operations on the beers.



## Technologies
The tech stack includes:
* MVVM architecture
* Retrofit for communication with the REST API
* Data Binding

## Features
 
 ### - Authentication:
 The application shows the UI for signing in and signing up. No real authentication has been implemented at this point. A password longer than four chars is needed for proceeding to the main layout.

 <table style="background-color: #708090;">
<tr>
<th>Login Layout :</th>
<th>Register Layout :</th>
</tr>
<td ><img src="screenshots/BeerTagLoginFin.png" alt="login layout" height="350"/></td>
<td ><img src="screenshots/BeerTagRegisterFin.png" alt="register layout" height="350"/></td>
</table>
<br>

### - Main Layout
The main layout presents a list of all of the beers from the database. They are presented in a Recycler View.<br>
The user can click on any of the beers to see more information about it, to add it to his lists of wished or drink beers.<br>
The user can also open a menu, giving him access to those lists or to his profile.

 <table style="background-color: #708090;">
<tr>
<th>Main Layout :</th>                             
<th>Main Layout With Open Menu :</th>
</tr> 
<td ><img src="screenshots/BeerTagMainViewFin.png" alt="main layout" height="350"/></td>
<td ><img src="screenshots/BeerTagMenyFin.png" alt="main layout wit open menu" height="350"/></td>
</table>
<br>


### - View Beer Information
If a user taps on a particular beer from the main layout, a new layot opens. There more info is provided about the beer.<br>
The user can also rate the beer, add it to or remove it from one of his lists of beers.

 <table style="background-color: #708090;">
<tr>
<th>Beer Information Layout :</th>                             
</tr> 
<td ><img src="screenshots/BeerTagBeerInfoViewFin.png" alt="beer info" height="350"/></td>
</table>
<br>


### - Create New Beer Information
The user also has the option to create a new beer in the database. On clicking the floating action button, the user is redirected to a layout,<br>
where he/she can provide the needed information and submit the beer for publishing in the database.


 <table style="background-color: #708090;">
<tr>
<th>Create New Beer Layout :</th>                             
</tr> 
<td ><img src="screenshots/BeerTagCreateBeerFin.png" alt="create new beer" height="350"/></td>
</table>
<br>